package StatDp.service;

import StatDp.Model.response.Someclass;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

@Service
public class StatServiceImpl implements StatService {

    private static final Logger logger = LogManager.getLogger(StatServiceImpl.class);
    @Value("${file.directory}")
    private String fileDirectory;

    /**
     * Возможные ошибки
     */
    private String errorNoResults = "{\"result\" : -1, \"message\" : \"no results in database\"}\n";
    private String errorEmptyPar = "{\"result\" : -1, \"message\" : \"input parameter is empty\"}\n";
    private String errorService = "{\"result\" : -1, \"message\" : \"service error\"}\n";
    private String errorSumm = "{\"result\" : -1, \"message\" : \"The money in the account is not enough\"}\n";
    private String ok = "\"result\" : 0, \"message\" : \"Оk!\"}\n";

//    ReBalance reBalance = new ReBalance();


    public Someclass xmltoclasss(String filename) throws Exception {
        File file = new File(filename);
        JAXBContext jaxbContext = JAXBContext.newInstance(Someclass.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (Someclass) jaxbUnmarshaller.unmarshal(file);
    }
}
