package StatDp.controller;


import StatDp.Model.response.Someclass;
import StatDp.service.StatService;
import StatDp.service.StatServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {
    private StatService statService;
    private static final Logger logger = LogManager.getLogger(StatServiceImpl.class);

    @Autowired
    public MainController(StatService statService) {
        this.statService = statService;
    }
    @Value("${file.directory}")
    private String fileDirectory;
    @Value("${login}")
    private String aa;
    @Value("${pass}")
    private String pas;
    //=====================================================
    @GetMapping("/xmltojson")
    public String main() {
        return "xmltojson";
    }

    //======================================================
    @PostMapping("/xmltojson")
    public String main(@RequestHeader("login") String logg, @RequestHeader("password") String pass) throws Exception {
        if((logg.equals(aa))&&(pass.equals(pas))) {
            Someclass someclass = statService.xmltoclasss(fileDirectory);
            System.out.println(someclass);
            ObjectMapper Obj = new ObjectMapper();
            return (Obj.writeValueAsString(someclass));
        }
        else
        {
            logger.info("error - 1, error_description - invalid authorization, file - "+fileDirectory);
            return ("ERROR");
        }
    }

//=================================================================



}
