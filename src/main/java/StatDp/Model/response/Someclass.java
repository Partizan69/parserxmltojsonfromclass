package StatDp.Model.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement
public class Someclass {



    private String Name;
    private String Street;
    private String Sity;
    private String State;
    private String Zip;
    private String Country;

    private String DeliveryNotes;
    private List<Someclass> Address;




    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getSity() {
        return Sity;
    }

    public void setSity(String sity) {
        Sity = sity;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    public String getCountry() {
        return Country;
    }


    public void setCountry(String country) {
        Country = country;
    }

    public String getDeliveryNotes() {
        return DeliveryNotes;
    }
    @XmlElement(name = "DeliveryNotes")
    public void setDeliveryNotes(String deliveryNotes) {
        DeliveryNotes = deliveryNotes;
    }

    public List<Someclass> getAddress() {
        return Address;
    }

    @XmlElementWrapper(name="address")
    @XmlElement(name = "someclass")
    public void setAddress(List<Someclass> address) {
        Address = address;
    }


}
